﻿using UnityEngine;

public class Asteroid : MonoBehaviour
{
    private int _size;
    private float _speed;

    public delegate void AsteroidDestroyHandler(Vector2 destructionPosition, int destructionSize);
    public event AsteroidDestroyHandler OnAsteroidDestroy;

    private void Update()
    {
        transform.Translate(Vector2.up * _speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            HandlePlayerCollision(collision);
        }
        else if (collision.CompareTag("Shot"))
        {
            HandleShotCollision(collision);
        }
    }

    private void HandlePlayerCollision(Collider2D collision)
    {
        bool isBlinking = false;
        Player player = collision.GetComponent<Player>();
        if (player != null)
        {
            isBlinking = player.IsBlinking();
            player.Damage();
        }
        if (!isBlinking)
        {
            DestroyAsteroid();
        }
    }

    private void HandleShotCollision(Collider2D collision)
    {
        Destroy(collision.gameObject);
        DestroyAsteroid();
    }

    public void SetSpeed(float speed)
    {
        _speed = speed;
    }

    public void SetSize(int size)
    {
        _size = size;
        Vector2 scale = new Vector2(_size, _size);
        transform.localScale = scale;
    }

    private void DestroyAsteroid()
    {
        Destroy(gameObject);
        OnAsteroidDestroy?.Invoke(gameObject.transform.position, _size);
    }
}
