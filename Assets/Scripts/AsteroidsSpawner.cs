﻿using UnityEngine;

public class AsteroidsSpawner : MonoBehaviour
{
    [SerializeField, Min(1)]
    private int _maxSize;
    [SerializeField, Min(1)]
    private int _minSize;
    [SerializeField, Min(1)]
    private int _sizeDecrement;

    [SerializeField, Min(1)]
    private int _maxSpawnsCount;
    [SerializeField, Min(1)]
    private int _minSpawnsCount;
    [SerializeField, Min(1)]
    private int _spawnsCountIncrement;
    [SerializeField, Range(0f, 1f)]
    private float _spawnPerimeterPercentage;

    [SerializeField, Min(1)]
    private int _maxDestructionParts;
    [SerializeField, Min(1)]
    private int _minDestructionParts;

    [SerializeField, Min(1f)]
    private float _maxSpeed;
    [SerializeField, Min(1f)]
    private float _minSpeed;

    [SerializeField]
    private GameObject[] _prefabs;

    private int _currentSpawnsCount;
    private Vector2[] _spawnPositions;
    private int _asteroidsCount;

    private void Awake()
    {
        _currentSpawnsCount = _minSpawnsCount;
        _spawnPositions = new Vector2[]
        {
            new Vector2(Random.Range(0f, 1f), Random.Range(1f - _spawnPerimeterPercentage, 1f)),
            new Vector2(Random.Range(1f - _spawnPerimeterPercentage, 1f), Random.Range(0f, 1f)),
            new Vector2(Random.Range(0f, 1f), Random.Range(0f, 0f + _spawnPerimeterPercentage)),
            new Vector2(Random.Range(0f, 0f + _spawnPerimeterPercentage), Random.Range(0f, 1f))
        };
    }

    private void Start()
    {
        SpawnMainAsteroids(_currentSpawnsCount);
    }

    private void SpawnMainAsteroids(int count)
    {
        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, 4);
            Vector2 position = Camera.main.ViewportToWorldPoint(_spawnPositions[index]);
            SpawnRandomAsteroid(position, _maxSize, _minSpeed);
        }
    }

    private void SpawnChildAsteroids(Vector2 parentPosition, int parentSize)
    {
        int count = Random.Range(_minDestructionParts, _maxDestructionParts + 1);
        for (int i = 0; i < count; i++)
        {
            float speed = Random.Range(_minSpeed, _maxSpeed);
            SpawnRandomAsteroid(parentPosition, parentSize, speed);
        }
    }

    private void SpawnRandomAsteroid(Vector2 position, int size, float speed)
    {
        if (_prefabs.Length > 0)
        {
            int index = Random.Range(0, _prefabs.Length);
            GameObject prefab = _prefabs[index];
            Quaternion rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            GameObject asteroidObject = Instantiate(prefab, position, rotation);
            Asteroid asteroid = asteroidObject.GetComponent<Asteroid>();
            if (asteroid != null)
            {
                _asteroidsCount++;
                asteroid.OnAsteroidDestroy += OnAsteroidDestroy;
                asteroid.SetSize(size);
                asteroid.SetSpeed(speed);
            }
        }
    }

    private void OnAsteroidDestroy(Vector2 destructionPosition, int destructionSize)
    {
        _asteroidsCount--;
        int childSize = destructionSize - _sizeDecrement;
        if (childSize >= _minSize)
        {
            SpawnChildAsteroids(destructionPosition, childSize);
        }
        if (_asteroidsCount <= 0)
        {
            _currentSpawnsCount = Mathf.Clamp(_currentSpawnsCount + _spawnsCountIncrement, _minSpawnsCount, _maxSpawnsCount);
            SpawnMainAsteroids(_currentSpawnsCount);
        }
    }
}
