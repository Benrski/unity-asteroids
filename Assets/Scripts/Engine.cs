﻿using System.Collections;
using UnityEngine;

public class Engine : MonoBehaviour
{
    private bool _turnedOn;

    private void Awake()
    {
        TurnOff();
    }

    public void TurnOn()
    {
        _turnedOn = true;
        IEnumerator enumerator = BlinkRandomly();
        StartCoroutine(enumerator);
    }

    public void TurnOff()
    {
        _turnedOn = false;
        Renderer renderer = gameObject.GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.enabled = false;
        }
    }

    private IEnumerator BlinkRandomly()
    {
        while (_turnedOn)
        {
            Renderer renderer = gameObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.enabled = true;
                yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
                renderer.enabled = false;
                yield return new WaitForSeconds(Random.Range(0.1f, 0.25f));
            }
            else
            {
                _turnedOn = false;
            }
        }
    }
}
