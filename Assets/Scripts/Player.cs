﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField, Min(1)]
    private int _lives;
    [SerializeField, Min(0f)]
    private float _blinkDuration;
    [SerializeField, Min(0f)]
    private float _blinkInterval;
    [SerializeField]
    private bool _isInvulnerableWhileBlinking;
    [SerializeField, Min(1f)]
    private float _maxRotationSpeed;
    [SerializeField, Min(1f)]
    private float _maxSpeed;

    private Engine _engine;
    private Rigidbody2D _rigidBody;
    private Renderer _renderer;
    private Shooter _shooter;
    private bool _isMoving;
    private bool _isBlinking;

    private void Awake()
    {
        _engine = GameObject.Find("Engine").GetComponent<Engine>();
        _shooter = GameObject.Find("Shooter").GetComponent<Shooter>();
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
        _renderer = gameObject.GetComponent<Renderer>();
    }

    private void Update()
    {
        CheckMovement();
        CheckShot();
    }

    private void FixedUpdate()
    {
        float speed = _isMoving ? _maxSpeed : 0f;
        _rigidBody.AddForce(transform.up * speed);
    }

    private void CheckMovement()
    {
        _isMoving = Input.GetKey(KeyCode.UpArrow);
        TurnOnEngine(_isMoving);
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.back, _maxRotationSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward, _maxRotationSpeed * Time.deltaTime);
        }
    }

    private void CheckShot()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shot();
        }
    }

    private void Shot()
    {
        if (_shooter != null)
        {
            _shooter.Shot();
        }
    }

    private void TurnOnEngine(bool turnOn)
    {
        if (_engine != null)
        {
            if (turnOn)
            {
                _engine.TurnOn();
            }
            else
            {
                _engine.TurnOff();
            }
        }
    }

    public void Damage()
    {
        if (!_isBlinking || !_isInvulnerableWhileBlinking)
        {
            _lives--;
            if (_lives <= 0)
            {
                Destroy(gameObject);
            }
            else
            {
                StartCoroutine(Blink());
            }
        }
    }

    public bool IsBlinking()
    {
        return _isBlinking;
    }

    private IEnumerator Blink()
    {
        _isBlinking = true;
        if (_renderer != null)
        {
            float endTime = Time.time + _blinkDuration;
            while (Time.time < endTime)
            {
                _renderer.enabled = false;
                yield return new WaitForSeconds(_blinkInterval);
                _renderer.enabled = true;
                yield return new WaitForSeconds(_blinkInterval);
            }
        }
        else
        {
            yield return null;
        }
        _isBlinking = false;
    }
}
