﻿using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField]
    private GameObject _shot;

    public void Shot()
    {
        Instantiate(_shot, transform.position, transform.rotation);
    }
}
