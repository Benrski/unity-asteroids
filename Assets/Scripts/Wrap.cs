﻿using UnityEngine;

public class Wrap : MonoBehaviour
{
    private bool _isWrappingX;
    private bool _isWrappingY;
    private Renderer[] _renderers;

    private void Awake()
    {
        _renderers = GetComponentsInChildren<Renderer>();
    }

    private bool IsRendererVisible()
    {
        foreach (Renderer renderer in _renderers)
        {
            if (renderer != null)
            {
                if (renderer.isVisible)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void Update()
    {
        WrapIfOut();
    }

    private void WrapIfOut()
    {
        bool isVisible = IsRendererVisible();
        if (isVisible)
        {
            _isWrappingX = false;
            _isWrappingY = false;
            return;
        }
        if (_isWrappingX && _isWrappingY)
        {
            return;
        }
        Vector2 vp = Camera.main.WorldToViewportPoint(transform.position);
        Vector2 newPosition = transform.position;
        if (!_isWrappingX && (vp.x > 1f || vp.x < 0f))
        {
            newPosition.x = -newPosition.x;
            _isWrappingX = true;
        }
        if (!_isWrappingY && (vp.y > 1f || vp.y < 0f))
        {
            newPosition.y = -newPosition.y;
            _isWrappingY = true;
        }
        transform.position = newPosition;
    }
}
